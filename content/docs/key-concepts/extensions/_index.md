---
bookCollapseSection: true
weight: 8
---


# Extensions

## Device Agents

Device agent is a software program installed on the hardware device that enables communication between the hardware device and Entgra IoT Server. For more information, see [Writing Device Agents](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-agents).

## Transport Extensions

Transport extensions enable you to establish a new communication channel between a device and Entgra IoT Server. For more information, see [Writing Transport Extensions](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-transport-extensions).

## Authentication Extensions

By default WOS2 IoT Server supports OAuth, basic auth, mutual SSL and certificate-based authentication mechanisms. If the new device types require some other authentication mechanism, authentication extensions can be used for this purpose.

## UI Extensions

This helps you to customize UIs of the new device type. For more information, see [Writing UI 
Extensions](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-ui-extensions). 
