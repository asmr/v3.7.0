---
bookCollapseSection: true
weight: 10
---

# <strong> Application Restriction Settings </strong>
{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to create a black list or white list of applications.

Application blacklisting, is a network administration practice used to prevent the execution of undesirable programs.  Such programs include not only those known to contain security threats or vulnerabilities but also those that are deemed inappropriate within a given organization.

Application whitelisting is the practice of specifying an index of approved software applications or executable files that are permitted to be present and active on a computer system. The goal of whitelisting is to protect computers and networks from potentially harmful applications.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Select type</strong></td>
            <td>Select the type of restriction to proceed.</td>
        </tr>
        <tr>
            <td colspan = "2"><strong><center>Restricted Application List</center></strong></td>
        </tr>
        <tr>
            <td><strong>Application Name/Description</strong></td>
            <td>Eg: [ Gmail ]</td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.gm ]</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}