---
bookCollapseSection: true
weight: 8
---

# <strong> ActiveSync Configurations </strong>

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to provision ActiveSync Configurations for iOS devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Email Address</strong></td>
            <td>Specifies the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.
            </td>
        </tr>
        <tr>
            <td><strong>Exchange Server Hostname</strong></td>
            <td>Specifies the Exchange server host name (or IP address).
            </td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Specifies whether the Exchange server uses SSL for authentication.</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>This string specifies the user name for this Exchange account.
                <br> Required in non-interactive installations (like MDM on iOS).</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>The password of the account. Use only with encrypted profiles.</td>
        </tr>
        <tr>
            <td><strong>Use OAuth</strong></td>
            <td>Specifies whether the connection should use OAuth for authentication. If enabled, a password should not be specified. This defaults to false.
                <br> Availability: Available only in iOS 12.0 and later.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Available in iOS only</strong>
                </center>
            </td>
        </tr>
        <tr>
            <td><strong>ActiveSync Certificate file</strong></td>
            <td>For accounts that allow authentication via certificate, a .p12 identity certificate in NSData blob format
            </td>
        </tr>
        <tr>
            <td><strong>Certificate Name</strong></td>
            <td> Specifies the name or description of the certificate
            </td>
        </tr>
        <tr>
            <td><strong>Certificate Password</strong></td>
            <td>The password necessary for the p12 identity certificate. Used with mandatory encryption of profiles.
            </td>
        </tr>
        <tr>
            <td><strong>Prevent Move</strong></td>
            <td>If set to true, messages may not be moved out of this email account into another account. Also prevents forwarding or replying from a different account than the message was originated from.
                <br> Availability: Available in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Prevent App Sheet</strong></td>
            <td>If set to true, this account will not be available for sending mail in any app other than the Apple Mail app.
                <br> Availability: Available in iOS 5.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Payload Certificate UUID</strong></td>
            <td>UUID of the certificate payload to use for the identity credential. If this field is present, the Certificate field is not used.
                <br> Availability: Available in iOS 5.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enabled</strong></td>
            <td> If true, this account supports S/MIME. As of iOS 10.0, this key is ignored.
                <br> Availability: Available only in iOS 5.0 through 9.3.3.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Enabled</strong></td>
            <td>If set to true, S/MIME signing is enabled for this account.
                <br> Availability: Available only in iOS 10.3 and later
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Certificate UUID</strong></td>
            <td> The PayloadUUID of the identity certificate used to sign messages sent from this account.
                <br> Availability: Available only in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Enabled</strong></td>
            <td>If set to true, S/MIME encryption is on by default for this account.
                <br> Availability: Available only in iOS 10.3 and later. As of iOS 12.0, this key is deprecated. It is recommended to use SMIMEEncryptByDefault instead.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Certificate UUID</strong></td>
            <td>The PayloadUUID of the identity certificate used to decrypt messages sent to this account. The public certificate is attached to outgoing mail to allow encrypted mail to be sent to this user. When the user sends encrypted mail, the public certificate is used to encrypt the copy of the mail in their Sent mailbox.
                <br> Availability: Available only in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enable PerMessage Switch</strong></td>
            <td>The password necessary for the p12 identity certificate. Used with mandatory encryption of profiles.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing User Overrideable</strong></td>
            <td>T If set to true, the user can toggle S/MIME signing on or off in Settings.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Certificate UUID UserOverrideable</strong></td>
            <td>If set to true, the user can select the signing identity.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encrypt By Default</strong></td>
            <td> If set to true, S/MIME encryption is enabled by default. If SMIMEEnableEncryptionPerMessageSwitch is false, this default cannot be changed by the user.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encrypt By Default User Overrideable</strong></td>
            <td> If set to true, the user can toggle the encryption by default setting.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Certificate UUID User Overrideable</strong></td>
            <td>If set to true, the user can select the S/MIME encryption identity and encryption is enabled.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enable Encryption Per-Message Switch</strong></td>
            <td>If set to true, displays the per-message encryption switch in the Mail Compose UI.
                <br> Availability: Available only in iOS 12.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow Mail drop</strong></td>
            <td>If true, this account is allowed to use Mail Drop. The default is false.
                <br> Availability: Available only in macOS 10.12 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Disable Mail Recents Syncing</strong></td>
            <td>If true, this account is excluded from address Recents syncing.
                <br> Availability: Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Mail Number Of PastDays To Sync</strong></td>
            <td>The number of days since synchronization.
            </td>
        </tr>
        <tr>
            <td><strong>Bundle ID of Default Application Handling Audio Calls</strong></td>
            <td>The communication service handler rules for this account. The CommunicationServiceRules dictionary currently contains only a DefaultServiceHandlers key; its value is a dictionary which contains an AudioCall key whose value is a string containing the bundle identifier for the default application that handles audio calls made to contacts from this account.
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}