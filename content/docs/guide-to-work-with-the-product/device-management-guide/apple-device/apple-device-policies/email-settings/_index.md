---
bookCollapseSection: true
weight: 4
---

# <strong> Email Settings </strong>

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

These configurations can be used to define settings for connecting to your POP or IMAP 
email accounts. Once this configuration profile is installed on an iOS device, corresponding 
users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>A user-visible description of the email account, shown in the Mail and Settings applications.
            </td>
        </tr>
        <tr>
            <td><strong>Account Type</strong></td>
            <td>Defines the protocol to be used for that account.</td>
        </tr>
        <tr>
            <td><strong>Path Prefix</strong></td>
            <td>The path prefix for the IMAP mail server</td>
        </tr>
        <tr>
            <td><strong>Email Account Name</strong></td>
            <td>The full user name for the account. This is the user name in sent messages, etc.
        </tr>
        <tr>
            <td><strong>Email Address</strong></td>
            <td>Designates the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Prevent move</strong></td>
            <td>If true, messages may not be moved out of this email account into another account. Also prevents forwarding or replying from a different account than the message was originated from.
                <br> Availability: Available only in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Prevent App Sheet</strong></td>
            <td>If true, this account is not available for sending mail in any app other than the Apple Mail app.
                <br> Availability: Available only in iOS 5.0 and later</td>
        </tr>
        <tr>
            <td><strong>Enable S/MIME</strong></td>
            <td> If true, this account supports S/MIME. As of iOS 10.0, this key is ignored.
                <br> Availability: Available only in iOS 5.0 through iOS 9.3.3.
            </td>
        </tr>
        <tr>
            <td><strong>S/MIME Signing Certificate UUID</strong></td>
            <td>The PayloadUUID of the identity certificate used to sign messages sent from this account.
                <br> Availability: Available only in iOS 5.0 and later</td>
        </tr>
        <tr>
            <td><strong>Enable Per-message Signing and Encryption Switch</strong></td>
            <td>If set to true, display the per-message encryption switch in the Mail Compose UI.
                <br> Availability: Available only in iOS 12.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow Recent Address Syncing</strong></td>
            <td>If true, this account is excluded from address Recents syncing. This defaults to false.
                <br> Availability: Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Incomming Mail Settings</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Mail Server Hostname</strong></td>
            <td>Designates the incoming mail server host name (or IP address).
            </td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Designates whether the incoming mail server uses SSL for authentication.</td>
        </tr>
        <tr>
            <td><strong>Mail Server Port</strong></td>
            <td> Designates the incoming mail server port number. If no port number is specified, the default port for a given protocol is used.</td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Designates the authentication scheme for incoming mail. Allowed values are EmailAuthPassword, EmailAuthCRAMMD5, EmailAuthNTLM, EmailAuthHTTPMD5, and EmailAuthNone.
            </td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>Designates the user name for the email account, usually the same as the email address up to the @ character. If not present in the payload, and the account is set up to require authentication for incoming email, the device will prompt for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td> Password for the Incoming Mail Server. Use only with encrypted profiles.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Outgoing Mail Settings</center></strong></td>
        </tr>
        <tr>
            <td><strong>Mail Server Hostname</strong></td>
            <td>Designates the outgoing mail server host name (or IP address).
            </td>
        </tr>
        <tr>
            <td><strong>Use Server Socket Layer(SSL)</strong></td>
            <td>Default false. Designates whether the outgoing mail server uses SSL for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Mail Server Port</strong></td>
            <td> Designates the outgoing mail server port number. If no port number is specified, ports 25, 587 and 465 are used, in this order.</td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Designates the authentication scheme for outgoing mail. Allowed values are EmailAuthPassword, EmailAuthCRAMMD5, EmailAuthNTLM, EmailAuthHTTPMD5, and EmailAuthNone.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>Designates the user name for the email account, usually the same as the email address up to the @ character. If not present in the payload, and the account is set up to require authentication for outgoing email, the device prompts for this string during profile installation</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td> Password for the Outgoing Mail Server. Use only with encrypted profiles.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}