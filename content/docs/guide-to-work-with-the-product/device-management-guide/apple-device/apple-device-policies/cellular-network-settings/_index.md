---
bookCollapseSection: true
weight: 11
---

# <strong> Cellular Network Settings </strong>

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

These configurations can be used to specify Cellular Network Settings on an iOS device. Cellular settings cannot be installed if an APN setting is already installed and upon successful installation, corresponding users will not be able to modify these settings on their devices.

<i> (This feature is supported only on iOS 7.0 and later.) </i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Cellular Configuration Name </strong></td>
            <td>The Access Point Name.
            </td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Must contain either CHAP or PAP. Defaults to PAP.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td> A user name used for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>A password used for authentication.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Configurations </strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>APN</strong></td>
            <td>The Access Point Name.
            </td>
        </tr>
        <tr>
            <td><strong>Auth.Type</strong></td>
            <td>Must contain either CHAP or PAP. Defaults to PAP.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>A user name used for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>A password used for authentication.</td>
        </tr>
        <tr>
            <td><strong>Proxy</strong></td>
            <td>The proxy serverʼs network address.</td>
        </tr>
        <tr>
            <td><strong>Port</strong></td>
            <td>The proxy serverʼs port.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}