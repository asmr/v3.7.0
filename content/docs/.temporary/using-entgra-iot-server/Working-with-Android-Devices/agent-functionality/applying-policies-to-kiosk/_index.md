# Applying policies to Kiosk

This section details how various policies can be set and applied to Kiosk devices.
This process is done via the generic policy management system with slight variations specific to each policy type.

## Creating a new policy

1. Go to the **Policy Management** view from the Top Menu

    ![image](1.png)

2. In the next page select **Add New Policy**
3. Creating a new policy comprises of several steps. The first of which is to select the platform. In the next page select **Android** as the platform.

    ![image](2.png)


### Change Kiosk display orientation

1. The next step involves **Profile Configuration**. It comprises of several different options. Scroll down until you find the **COSU Profile Configuration** and toggle the switch on the upper right corner to activate it.

    ![image](3.png)

2. The option **Device Global Configuration** would be unticked by default. Tick it so that additional options will be listed below it.

    ![image](4.png)

3. Scroll down till you find the field titled; **Device display orientation**. Set it to *Auto* in the drop down.
Press **Continue** to move into subsequent steps.

    ![image](5.png)

2. Edit the following steps accordingly.


### Add custom themeing to Kiosk devices

1. The next step involves **Profile Configuration**. It comprises of several different options. Scroll down until you find the **COSU Profile Configuration** and toggle the switch on the upper right corner to activate it.

    ![image](3.png)

2. The option **Device Global Configuration** would be unticked by default. Tick it so that additional options will be listed below it.

    ![image](4.png)

3. Immediately below it you will find all the fields related to theming. Fill the respective fields accordingly.

    ![image](7.png)


### Display video or image on Kiosk idle

1. The next step involves **Profile Configuration**. It comprises of several different options. Scroll down until you find the **COSU Profile Configuration** and toggle the switch on the upper right corner to activate it.

    ![image](3.png)

2. The option **Device Global Configuration** would be unticked by default. Tick it so that additional options will be listed below it.

    ![image](4.png)

3. Scroll down until you come across the field named **Is Idle Media enabled**.
Tick the checkbox next to it to activate.

    ![image](8.png)

4. Fill the respective fields accordingly.

## Save and Publish

Finally, once all settings are satisfactory, press **Save & Publish** to execute the policy onto the enrolled devices.


![image](6.png)